# README #


### What is this repository for? ###

* Files required to meta-analyze efficacy and safety outcomes in the large SGLT2i outcome trials
* 1.0


### How do I get set up? ###

* There are three R script files in this directory which create the figures
* Any R installation is required to run these files
* Dependencies: R packages xlsx and meta
* Simply run the R scripts to generate the images !!


### Who do I talk to? ###

* Christos Argyropoulos MD, cargyropoulos@salud.unm.edu
